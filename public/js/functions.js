var stringify = function(x, fixed) {
  fixed = fixed || 2;
  if(typeof(x) != 'number') throw 'asd';
  var str = Math.abs(x) > 1000 ? x.toExponential(fixed) : x.toFixed(fixed);
  if((str*1) == 0) str = '0';
  
  return str;
};
var functions = {
  /*------------------------------------------------------------------------------------*/
  'polynomial': {
    label: 'Polynomial',
    max_parameters: 0,
    dydx: function(x) {
      var index,
          parameters = this.get('parameters'),
          y=0;
      
      // build the value
      parameters.each(function(parameter, index) {
        if(index != 0) {
          y += parameter.get('value') * index * Math.pow(x, index-1);
        }
      });
      
      return y;
    },
    getDistribution: function() {
      var domain = this.getVisibleDomain(-400, 400, -200, 200);
      return jStat.normal((domain[0] + domain[1])/2, (1/4) * (domain[1] - domain[0]));
    },
    inDomain: function(x) {
      return true;
    },
    inRange: function(y) {
      return true;
    },
    initialBounds: function() {
      // in general, no asymmetries (although there could be)
      return {'x1':-400, 'x2':400, 'y1':-400, 'y2':400};
    },
    map: function(x) {
      var index,
        parameters = this.get('parameters'),
        y=0;
      
      // build the value
      parameters.each(function(parameter, index) {
        y += parameter.get('value') * Math.pow(x, index);
      });
      
      return y;
    },
    // @param data an array of [x,y] coordinates: e.g. [[0,1], [1,1], [1,2]]
    regress: function(data) {
      var parameters = this.get('parameters'), y, X = [], terms = [];
      
      // get the raw vectors
      y = _.pluck(data, 1);
      x = _.pluck(data, 0);
      
      // get the string names for the terms
      terms[0] = this.term('y');
      
      // get the matrix of columns of x data
      // even if there are no parameters, we're guaranteed at least a constant term
      j=0;
      do {
        X.push(_.map(x, function(e) { return Math.pow(e, j); }))
        terms[j+1] = this.term(j);
      } while(++j < parameters.length);
      
      return new lm(y, X, terms);
    },
    term: function(i) {
      var str;
      if (i == 'y') {
        str = 'y';
      }
      else if(i == 0) {
        str = '_cons';
      }
      else if (i == 1) {
        str = 'x';
      }
      else {
        str = 'x<sup>'+i+'</sup>';
      }
      return str;
    },
    toString: function(type) {
      var str = '', fnc = '', index, parameters, value, x, y, m, b;;
      type = type || 'map';
      
      parameters = this.get('parameters');
      switch(type) {
        case 'map':
          fnc = "&fnof;(x) = ";
          // build the value
          parameters.each(function(parameter, index) {
            value = parameter.get('value');
            
            // only display something if the value isn't 0
            if(value != 0) {
              if(str != '') str += ' + ';
            
              // for the constant term (x^0)
              if(index == 0) {
                str += stringify(value);
              }
              // for x
              else if(index == 1) {
                str += (value == 1 ? '' : stringify(value));
                str += 'x';
              }
              // for x^2, x^3, ...
              else {
                str += (value == 1 ? '' : stringify(value));
                str += 'x<sup>'+index+'</sup>';
              }
            }
          });
          break;
        case 'dydx':
          fnc = "&fnof;'(x) = ";
          // build the value
          parameters.each(function(parameter, index) {
            value = parameter.get('value') * index;
            
            // only display something if the value isn't 0
            if(value != 0) {
              if(str != '') str += ' + ';
              
              // d(a)/dx = 0
              if(index == 0) {
                // do nothing
              }
              // d(bx)/dx = b
              else if(index == 1) {
                str += stringify(value);
              }
              // d(cx^2)/dx = 2cx
              else if(index == 2) {
                str += stringify(value);
                str += 'x';
              }
              // d(dx^n)/dx = n*2dx^n-1
              else {
                str += stringify(value);
                str += 'x<sup>'+(index-1)+'</sup>';
              }
            }
            
          });
          break;
        case 'tangent':
          fnc = "t(x) = ";
          x = this.get('tangent_point');
          y = this.map(x);  // y-value
          m = this.dydx(x); // slope
          b = y - m*x;      // y-intercept
          
          m = stringify(m);
          b = stringify(b);
          
          if(b != '0') str += b;
          if(str != '' && m != '0') str += ' + ';
          if(m != '0') str += (m*1 == 1 ? '' : m)+'x';
          
          break;
      }
      if(str == '') str = '0';
      
      return fnc + str;
    }
  },
  /*------------------------------------------------------------------------------------*/
  'hyperbola': {
    label: 'Hyperbola',
    max_parameters: 0,
    domain: function(a, b, setting) {
      var points, parameters = this.get('parameters');
      setting = setting || 'static'; // possible values: static, dynamic, zoom
      points = (setting == 'static') ? 1000 : 1000;
      
      a = ((b-a)/(points*10));
      
      return _.range(a, b, Math.abs(b-a)/points);
    },
    dydx: function(x) {
      var index,
          parameters = this.get('parameters'),
          y=0;
      
      // build the value
      parameters.each(function(parameter, index) {
        if(index != 0) {
          y += parameter.get('value') * (-index) * Math.pow(x, -(index+1));
        }
      });
      
      return y;
    },
    getDistribution: function() {
      return jStat.exponential(0.04);
    },
    inDomain: function(x) {
      return x > 0;
    },
    inRange: function(y) {
      return true;
    },
    initialBounds: function() {
      var parameters = this.get('parameters');
      // primary asymmetry around y-axis
      return {'x1':0.0001, 'x2':5, 'y1':0, 'y2':400};
    },
    map: function(x) {
      var index,
        parameters = this.get('parameters'),
        y=0;
      
      // build the value
      parameters.each(function(parameter, index) {
        y += parameter.get('value') * Math.pow(x, -index);
      });
      
      return y;
    },
    // @param data an array of [x,y] coordinates: e.g. [[0,1], [1,1], [1,2]]
    regress: function(data) {
      var parameters = this.get('parameters'), y, X = [], terms = [];
      
      // get the raw vectors
      y = _.pluck(data, 1);
      x = _.pluck(data, 0);
      
      // get the string names for the terms
      terms[0] = this.term('y');
      
      // get the matrix of columns of x data
      // even if there are no parameters, we're guaranteed at least a constant term
      j=0;
      do {
        X.push(_.map(x, function(e) { return Math.pow(e, -j); }))
        terms[j+1] = this.term(j);
      } while(++j < parameters.length);
      
      return new lm(y, X, terms);
    },
    term: function(i) {
      var str;
      if (i == 'y') {
        str = 'y';
      }
      else if(i == 0) {
        str = '_cons';
      }
      else {
        str = 'x<sup>'+(-i)+'</sup>';
      }
      return str;
    },
    toString: function(type) {
      var str = '', fnc = '', index, parameters, value, x, y, m, b;
      type = type || 'map';
      
      parameters = this.get('parameters');
      switch(type) {
        case 'map':
          fnc = "&fnof;(x) = ";
          // build the value
          parameters.each(function(parameter, index) {
            value = parameter.get('value');
            if(value != 0) {
              if(str != '') str += ' + ';
              
              // for the constant term (x^0)
              if(index == 0) {
                str += stringify(value);
              }
              // for x^-n
              else {
                str += (value == 1 ? '' : stringify(value));
                str += 'x<sup>'+(-index)+'</sup>';
              }
            }
          });
          break;
        case 'dydx':
          fnc = "&fnof;'(x) = ";
          // build the value
          parameters.each(function(parameter, index) {
            value = parameter.get('value') * -index;
            if(value != 0) {
              if(str != '') str += ' + ';
              
              // the constant term goes away
              if(index == 0) {
                // do nothing
              }
              // ax^-n -> -n * a * x^(-n-1)
              else {
                str += (value == 1 ? '' : stringify(value));
                str += 'x<sup>'+(-index-1)+'</sup>';
              }
              
            }
          });
          break;
        case 'tangent':
          fnc = "t(x) = ";
          x = this.get('tangent_point')
          y = this.map(x);  // y-value
          m = this.dydx(x); // slope
          b = y - m*x;      // y-intercept
          
          m = stringify(m);
          b = stringify(b);
          
          if(b != '0') str += b;
          if(str != '' && m != '0') str += ' + ';
          if(m != '0') str += (m*1 == 1 ? '' : m)+'x';
          break;
      }
      if(str == '') str = '0';
      
      return fnc + str;
    }
  },
  /*------------------------------------------------------------------------------------*/
  'log_linear': {
    label: 'Log-Linear',
    max_parameters: 0,
    dydx: function(x) {
      var y = 0, parameters = this.get('parameters');
      
      // first take the chain rule (i.e. derivative of the inside)
      parameters.each(function(parameter, index) {
        if(index != 0) {
          y += parameter.get('value') * index * Math.pow(x, index-1);
        }
      });
      
      // and multiply by the derivative of the outside
      return y * this.map(x);
    },
    getDistribution: function() {
      var domain = this.getVisibleDomain(-400, 400, -200, 200);
      return jStat.normal((domain[0] + domain[1])/2, (1/4) * (domain[1] - domain[0]));
    },
    inDomain: function(x) {
      return true;
    },
    inRange: function(y) {
      return y > 0;
    },
    initialBounds: function() {
      // in general, no asymmetries (although there could be)
      return {'x1':-400, 'x2':400, 'y1':-400, 'y2':400};
    },
    map: function(x) {
      var index,
        parameters = this.get('parameters'),
        y=0;
      
      // build the value
      parameters.each(function(parameter, index) {
        y += parameter.get('value') * Math.pow(x, index);
      });
      
      return Math.exp(y);
    },
    // @param data an array of [x,y] coordinates: e.g. [[0,1], [1,1], [1,2]]
    regress: function(data) {
      var parameters = this.get('parameters'), y, X = [], terms = [];
      
      // get the raw vectors
      y = _.map(_.pluck(data, 1), Math.log);
      x = _.pluck(data, 0);
      
      // get the string names for the terms
      terms[0] = this.term('y');
      
      // get the matrix of columns of x data
      // even if there are no parameters, we're guaranteed at least a constant term
      j=0;
      do {
        X.push(_.map(x, function(e) { return Math.pow(e, j); }))
        terms[j+1] = this.term(j);
      } while(++j < parameters.length);
      
      return new lm(y, X, terms);
    },
    term: function(i) {
      var str;
      if (i == 'y') {
        str = 'ln(y)';
      }
      else if(i == 0) {
        str = '_cons';
      }
      else if (i == 1) {
        str = 'x';
      }
      else {
        str = 'x<sup>'+i+'</sup>';
      }
      return str;
    },
    toString: function(type) {
      var str = '', fnc = '', index, parameters, value, x, y, m, b;
      type = type || 'map';
      
      parameters = this.get('parameters');
      switch(type) {
        case 'map':
          fnc = "ln(y) = ";
          // build the value
          parameters.each(function(parameter, index) {
            value = parameter.get('value');
            
            // only display something if the value isn't 0
            if(value != 0) {
              if(str != '') str += ' + ';
            
              // for the constant term (x^0)
              if(index == 0) {
                str += stringify(value);
              }
              // for x
              else if(index == 1) {
                str += (value == 1 ? '' : stringify(value));
                str += 'x';
              }
              // for x^2, x^3, ...
              else {
                str += (value == 1 ? '' : stringify(value));
                str += 'x<sup>'+index+'</sup>';
              }
            }
          });
          break;
        case 'dydx':
          fnc = "dln(y) / dx = ";
          // build the value
          parameters.each(function(parameter, index) {
            value = parameter.get('value') * index;

            // only display something if the value isn't 0
            if(value != 0) {
              if(str != '') str += ' + ';
              
              // d(a)/dx = 0
              if(index == 0) {
                // do nothing
              }
              // d(bx)/dx = b
              else if(index == 1) {
                str += stringify(value);
              }
              // d(cx^2)/dx = 2cx
              else if(index == 2) {
                str += stringify(value);
                str += 'x';
              }
              // d(dx^n)/dx = n*2dx^n-1
              else {
                str += stringify(value);
                str += 'x<sup>'+(index-1)+'</sup>';
              }
            }

          });
          break;
        case 'tangent':
          fnc = "t(x) = ";
          x = this.get('tangent_point')
          y = this.map(x);  // y-value
          m = this.dydx(x); // slope
          b = y - m*x;      // y-intercept
          
          m = stringify(m);
          b = stringify(b);
          
          if(b != '0') str += b;
          if(str != '' && m != '0') str += ' + ';
          if(m != '0') str += (m*1 == 1 ? '' : m)+'x';
          break;
      }
      if(str == '') str = '0';
      
      return fnc + str;
    }
  },
  /*------------------------------------------------------------------------------------*/
  'linear_log': {
    label: 'Linear-Log',
    max_parameters: 2,
    domain: function(a, b, setting) {
      var points = setting == 'static' ? 1000 : 300;
      return _.range(0.0000001, b, Math.abs(b)/points);
    },
    dydx: function(x) {
      var parameters = this.get('parameters');
      return parameters.length > 1 ? parameters.at(1).get('value') / x : 0;
    },
    getDistribution: function() {
      return dist = jStat.exponential(0.04);
    },
    inDomain: function(x) {
      return x > 0;
    },
    inRange: function(y) {
      return true;
    },
    initialBounds: function() {
      // primary asymmetry around y-axis
      return {'x1':0.0001, 'x2':400, 'y1':-400, 'y2':400};
    },
    map: function(x) {
      var parameters = this.get('parameters'), a, b;
      
      a = parameters.length > 0 ? parameters.at(0).get('value') : 0;
      b = parameters.length > 1 ? parameters.at(1).get('value') * Math.log(x) : 0;
      
      if(isNaN(a) || a == -Infinity || a == Infinity) a = 0;
      if(isNaN(b) || b == -Infinity || b == Infinity) b = 0;
      
      return a + b;
    },
    // @param data an array of [x,y] coordinates: e.g. [[0,1], [1,1], [1,2]]
    regress: function(data) {
      var parameters = this.get('parameters'), y, X = [], terms = [];
      
      // get the raw vectors
      y = _.pluck(data, 1);
      x = _.pluck(data, 0);
      
      // get the string names for the terms
      terms[0] = this.term('y');
      terms[1] = this.term(0);
      terms[2] = this.term(1);
      
      // get the matrix of columns of x data
      // even if there are no parameters, we're guaranteed at least a constant term
      X.push(_.map(x, function(e) { return 1; }));
      if(parameters.length > 1) {
        X.push(_.map(x, Math.log));
      }
      
      return new lm(y, X, terms);
    },
    term: function(i) {
      var str;
      if (i == 'y') {
        str = 'y';
      }
      else if(i == 0) {
        str = '_cons';
      }
      else if (i == 1) {
        str = 'ln(x)';
      }
      return str;
    },
    toString: function(type) {
      var str = '', fnc = '', index, parameters, value, x, y, m, b;
      type = type || 'map';
      
      parameters = this.get('parameters');
      switch(type) {
        case 'map':
          fnc = "&fnof;(x) = ";
          
          // constant term
          if(parameters.length > 0) {
            str += stringify(parameters.at(0).get('value'));
          }
          
          // logged term
          if(parameters.length > 1) {
            str += ' + '; // value still refers to the constant term
            value = parameters.at(1).get('value'); 
            str += (value != 1 ? stringify(value) : '');
            if(value != 0) str += 'ln(x)';
          }
             
          break;
        case 'dydx':
          fnc = "&fnof;'(x) = ";
          
          // constant term goes away
          
          // logged term goes to 1/x
          if(parameters.length > 1) {
            value = parameters.at(1).get('value');
            str += (value != 1 ? stringify(value) : '') +'x<sup>-1</sup>';
          }
          break;
        case 'tangent':
          fnc = "t(x) = ";
          x = this.get('tangent_point')
          y = this.map(x);  // y-value
          m = this.dydx(x); // slope
          b = y - m*x;      // y-intercept
          
          m = stringify(m);
          b = stringify(b);
          
          if(b != '0') str += b;
          if(str != '' && m != '0') str += ' + ';
          if(m != '0') str += (m*1 == 1 ? '' : m)+'x';
          break;
      }
      if(str == '') str = '0';
      
      return fnc + str;
    }
  },
  /*------------------------------------------------------------------------------------*/
  'log_log': {
    label: 'Log-Log',
    max_parameters: 2,
    domain: function(a, b, setting) {
      var points = setting == 'static' ? 1000 : 300;
      return _.range(0.0000001, b, Math.abs(b)/points);
    },
    dydx: function(x) {
      var y = 0, parameters = this.get('parameters');
      
      // first take the chain rule (i.e. derivative of the inside)
      if(parameters.length > 1) {
        y += parameters.at(1).get('value') / x;
      }
      
      // and multiply by the derivative of the outside
      return y * this.map(x);
    },
    getDistribution: function() {
      return dist = jStat.exponential(0.04);
    },
    inDomain: function(x) {
      return x > 0;
    },
    inRange: function(y) {
      return y > 0;
    },
    initialBounds: function() {
      // primary asymmetry around y-axis
      return {'x1':0.0001, 'x2':400, 'y1':-400, 'y2':400};
    },
    map: function(x) {
      var parameters = this.get('parameters'), a, b;
      
      a = parameters.length > 0 ? parameters.at(0).get('value') : 0;
      b = parameters.length > 1 ? parameters.at(1).get('value') * Math.log(x) : 0;
      
      if(isNaN(a) || a == -Infinity || a == Infinity) a = 0;
      if(isNaN(b) || b == -Infinity || b == Infinity) b = 0;
      
      return Math.exp(a + b);
      
      // build the value
      parameters.each(function(parameter, index) {
        y += parameter.get('value') * Math.pow(x, index);
      });
      
      return Math.exp(y);
    },
    // @param data an array of [x,y] coordinates: e.g. [[0,1], [1,1], [1,2]]
    regress: function(data) {
      var parameters = this.get('parameters'), y, X = [], terms = [];
      
      // get the raw vectors
      y = _.map(_.pluck(data, 1), Math.log);
      x = _.pluck(data, 0);
      
      // get the string names for the terms
      terms[0] = this.term('y');
      terms[1] = this.term(0);
      terms[2] = this.term(1);
      
      // get the matrix of columns of x data
      // even if there are no parameters, we're guaranteed at least a constant term
      X.push(_.map(x, function(e) { return 1; }));
      if(parameters.length > 1) {
        X.push(_.map(x, Math.log));
      }
      
      return new lm(y, X, terms);
    },
    term: function(i) {
      var str;
      if (i == 'y') {
        str = 'ln(y)';
      }
      else if(i == 0) {
        str = '_cons';
      }
      else if (i == 1) {
        str = 'ln(x)';
      }
      return str;
    },
    toString: function(type) {
      var str = '', fnc = '', index, parameters, value, x, y, m, b;
      type = type || 'map';
      
      parameters = this.get('parameters');
      switch(type) {
        case 'map':
          fnc = "ln(y) = ";
          
          // constant term
          if(parameters.length > 0) {
            str += stringify(parameters.at(0).get('value'));
          }
          
          // logged term
          if(parameters.length > 1) {
            str += ' + ';
            value = parameters.at(1).get('value'); 
            str += (value != 1 ? stringify(value) : '')
            if(value != 0) str += 'ln(x)';
          }
             
          break;
        case 'dydx':
          fnc = "dln(y) / dx = ";
          
          // constant term goes away
          
          // logged term goes to 1/x
          if(parameters.length > 1) {
            value = parameters.at(1).get('value');
            str += (value != 1 ? stringify(value) : '') +'x<sup>-1</sup>';
          }
          break;
        case 'tangent':
          fnc = "t(x) = ";
          x = this.get('tangent_point')
          y = this.map(x);  // y-value
          m = this.dydx(x); // slope
          b = y - m*x;      // y-intercept
          
          m = stringify(m);
          b = stringify(b);
          
          if(b != '0') str += b;
          if(str != '' && m != '0') str += ' + ';
          if(m != '0') str += (m*1 == 1 ? '' : m)+'x';
          break;
      }
      if(str == '') str = '0';
      
      return fnc + str;
    }
  }
};