/**
 * Models
 */
var parameter = Backbone.Model.extend({
  defaults: { value: 0 }
});

var functional = Backbone.Model.extend({
  defaults: function() {
    return {
      color: 'black',
      max_parameters: 0,
      parameters: new parameters,
      type:'generic'
    }
  },
  domain:   function() { return 1000; },
  dydc:     function() { return; }, // don't graph anything by default
  map:      function() { return; }, // don't graph anything by default
  range:    function() { return 1000; },
  step:     function() { return 1; },
  tangent:  function() { return; }, // don't graph anything by default
  tex: {
    func: '',
    dydx: ''
  }
});

/**
 * Collections
 */
var parameters = Backbone.Collection.extend({
  model: parameter
});

var functionals = Backbone.Collection.extend({
  model: functional
});

 
/**
 * Views
 */
var parameterSettings = Backbone.View.extend({
  className: "parameter",
  
  //template: _.template($('#parameter-template').html()),
  
  events: {
  },
  
  render: function() {
    return this;
  }
});

var functionalSettings = Backbone.View.extend({
  className: "functional",
  template: _.template($('#function-template').html()),
  events: {},
  
  initialize: function() {
    
  },
  
  render: function() {
    this.$el.append(this.template({model: this.model}));
    return this;
  }
});

var functionalSVG = Backbone.View.extend({
  initialize: function() {    
    var path, data, self = this;
    
    // get the data
    data = _.filter(
      _.map(this.model.domain(), function(x) { return { x:x, y:this.model.map(x) }; }, this),
      function(x) {
        return Math.abs(x.y) <= this.model.range();
      },
      this
    );
    
    // draw the path
    path = this.options.d3.curves
                          .data([data])
                            .append('path')
                          .attr('class', 'line curve')
                          .style('stroke', self.model.get('color'))
                          .attr(
                            'd',
                            d3.svg.line().x(function(d) {
                              return self.options.d3.xScale0(d.x);
                            }).y(function(d) {
                              return self.options.d3.yScale0(d.y);
                            })
                          );

    // we need to manually set the element, since jQuery appends using innerHTML(), which
    // does not work with svg namespaced elements like svg:g.
    this.setElement(path[0]);
  },
  
  render: function() {
    var data, self = this;
    // get the new data
    data = _.filter(
      _.map(this.model.domain(), function(x) { return { x:x, y:this.model.map(x)+1 }; }, this),
      function(x) {
        return Math.abs(x.y) <= this.model.range();
      },
      this
    );
    
    // update the path
    d3.select(this.el)
        .data([data])
      .transition()
        .duration(10000)
        .attr(
          'd',
          d3.svg.line().x(function(d) {
            return self.options.d3.xScale0(d.x);
          }).y(function(d) {
            return self.options.d3.yScale0(d.y);
          })
        );
           
    return this;
  }
});
