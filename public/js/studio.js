var graph, funcs, studio, incompatible = false;

/**
 * Graph Model (for the graph pane)
 *
 * There will only be one of these.
 */
var GraphModel = Backbone.Model.extend({
  defaults: function() {
    return {
      'dim': {
        w: 700,
        h: 560,
        m: 0
      },
      'xRange': [-5,5],
      'yRange': [-5,5],
      'scaleExtent': [0.01, 10000],
      'tickSubdivide': true
    }
  }
});

/* ----------------------------------------------------------------------------------- */

/**
 * 2D Function Studio Top-Level View
 */
var StudioView = Backbone.View.extend({
  el: $('#studio'),
  
  cache: { // cache of HTML elements
    'add_functional': null,
    'coordinates': null,
    'xRange': null,
    'yRange': null,
    'xSlider': null,
    'ySlider': null
  },
  
  counter: 0, // counter of zoom / drag events
  
  d3: { // d3 graph-related elements
    'xScale': null,
    'yScale': null,
    'xScale0': null,
    'yScale0': null,
    'xAxis': null,
    'yAxis': null,
    'xSpine': null,
    'ySpine': null,
    'svg': null,
    'g': null,
    'curves': null,
    'zoom': null
  },
  
  events: {
    'click #add-functional': 'create',
    'mousemove svg': 'move'
  },
  
  initialize: function() {
    this.initializeCache();
    this.initializeSVG();
    this.initializeFunctionals();
    
    this.render();
  },
  
  /**
   * Cache page HTML elements
   */
  initializeCache: function() {
    this.cache.coordinates = this.$('#coordinates');
    this.cache.xRange = this.$('#x-axis');
    this.cache.yRange = this.$('#y-axis');
    this.cache.xSlider = this.$('.slider', this.cache.xRange);
    this.cache.ySlider = this.$('.slider', this.cache.yRange);
  },
  
  /**
   * Create the d3 SVG elements
   */
  initializeSVG: function() {
    var self = this, g = this.model.toJSON();
  
    // create the graph's scales
    this.d3.xScale = d3.scale.linear().domain(g.xRange)
                                      .range([0, g.dim.w - (g.dim.m*2)]);
    this.d3.yScale = d3.scale.linear().domain(g.yRange)
                                      .range([g.dim.h - (g.dim.m*2), 0]);
                                      
    // make copies of the original scales, because the zoom activity will change the
    // domains of the scales, but zoom's translation does not then reset to [0,0],
    // so we'll always need to draw the path elements against the original scales (while
    // their containing group elements will be what is translated) to maintain the right
    // translations when adjusting function graphs
    this.d3.xScale0 = this.d3.xScale.copy()
    this.d3.yScale0 = this.d3.yScale.copy()
                                      
    // create the axis objects
    this.d3.xAxis = d3.svg.axis()
                  .scale(this.d3.xScale)
                  .tickSize(4, 2, 0)
                  .tickSubdivide(true);
    this.d3.yAxis = d3.svg.axis()
                  .orient('left')
                  .scale(this.d3.yScale)
                  .tickSize(4, 2, 0)
                  .tickSubdivide(true);
    
    // set up zooming / panning
    this.d3.zoom = d3.behavior.zoom()
                              .x(this.d3.xScale)
                              .y(this.d3.yScale)
                              .scaleExtent(g.scaleExtent)
                              .on("zoom", function() {
                                self.render(d3.event.sourceEvent.type == 'dblclick' ? 350 : 0);
                              });
    // create the svg container
    this.d3.svg = d3.select('#graph').append('svg')
                                     .attr("width", g.dim.w)
                                     .attr("height", g.dim.h)
                                     .call(this.d3.zoom);
    // and the main group
    this.d3.g = this.d3.svg.append('g')
                   .attr("transform", "translate(" + g.dim.m + "," + g.dim.m + ")");
    
    // display the axes spines
    this.d3.xSpine = this.d3.g.append("g")
                              .attr("class", "x axis")
                              .attr("transform", "translate(" + 0 + "," + this.d3.yScale(0) + ")")
                              .call(this.d3.xAxis);
    this.d3.ySpine = this.d3.g.append("g")
                              .attr("class", "y axis")
                              .attr("transform", "translate(" + this.d3.xScale(0) + "," + 0 + ")")
                              .call(this.d3.yAxis);
                              
    // create the curves
    this.d3.curves = this.d3.g.append('g').attr('class', 'curves');
    this.d3.datapoints = this.d3.g.append('g').attr('class', 'datapoints');
  },
  
  initializeFunctionals: function() {
    // Cache and initialize the add-functional button
    this.cache.functionals = this.$('#functionals');
    this.cache.add_functional = this.$('#add-functional');
    this.cache.add_functional.button();
    
    // Add any provided functionals
    this.collection.each(this.add, this);
    
    // Bind events to the functionals collection
    this.collection.bind('add', this.add, this);
  },
  
  render: function(duration) {
    var self = this, t, translate, scale, curves, dp;
    duration = duration || 0;
    
    this.d3.translate = translate = d3.event ? d3.event.translate : 0;
    this.d3.scale = scale = d3.event ? d3.event.scale : 1;
    
    /* Update the axis */
    if(duration) {
      t = this.d3.svg.transition().duration(duration);
    }
    else {
      t = this.d3.svg;
    }
    // move the y-axis to the new position of x=0
    t.select(".y.axis").attr("transform", "translate(" + this.d3.xScale(0) + "," + 0 + ")")
    // move the x-axis to the new position of y=0
    t.select(".x.axis").attr("transform", "translate(" + 0 + "," + this.d3.yScale(0) + ")")
    // change the x-axis to the new range
    t.select(".x.axis").call(this.d3.xAxis)
    // change the y-axis to the new range
    t.select(".y.axis").call(this.d3.yAxis)
    
    /* Update the group / paths */
    // we have to treat drag, and zoom via mousewheel differently than zoom via
    // double-click, because the transition in the stroke-width means that if the path is
    // re-rendered here, the stroke-width property hasn't been set yet, so when the path
    // is re-drawn, it doesn't correctly set the stroke-width property.
    if(duration == 0 && ((this.counter++ % 3) == 0 || Math.abs(d3.event.sourceEvent.wheelDelta) > 400)) {
      self.collection.each(function(d, i) {
          d.trigger('zoom');
        });
    }
    
    curves = d3.selectAll('g.curves');
    dp = d3.selectAll('g.datapoints');
    
    // if there was a double-click, so we need a transition
    if(duration) {
      curves.transition().duration(duration)
          .attr("transform", "translate(" + translate + ")" + " scale(" + scale + ")")
        .selectAll('path.function')
          .style('stroke-width', 1.1/scale);
      dp.transition().duration(duration)
          .attr("transform", "translate(" + translate + ")" + " scale(" + scale + ")")
        .selectAll('text.dp')
          .style('font-size', 9/scale+"px");
    }
    // otherwise (zoom / pan) no transition needed
    else {
      curves.attr("transform", "translate(" + translate + ")" + " scale(" + scale + ")")
        .selectAll('path.function')
          .style('stroke-width', 1.1/scale);
      dp.attr("transform", "translate(" + translate + ")" + " scale(" + scale + ")")
        .selectAll('text.dp')
          .style('font-size', 9/scale+"px");
    }
    
    d3.selectAll('path.dydx')
        .style('stroke-width', 0.2/scale);
    d3.selectAll('line.curve')
      .style('stroke-width', 1.1/scale);
    d3.selectAll('circle.tangent')
      .attr('r', 3/scale);
      
    if(duration != 0) {
      setTimeout(function() {
        self.collection.each(function(d, i) {
          d.trigger('zoom');
        });
      }, duration);
    }
  },
  
  // create a new functional (from the "+ Function" button)
  create: function(e) {
    e.preventDefault();
    this.collection.add();
  },
  
  /**
   * add a functional to the SVG graph
   * @param showSettings on manual calls (i.e. not Backbone's each() or bind() calls)
   *                     we may not want to show the settings view (e.g. for regressions)
   *                     so we overload the second parameter (which is the index in an
   *                     each() call)
   */
  add: function(functional, showSettings) {
    var settings, view, index = 0;
    if(showSettings !== false) {
      index = showSettings;
      showSettings = true;
    }
  
    /* Add the functional settings view (parameters, etc) */
    if(showSettings) {
      settings = new functionalSettings({model: functional});
      this.cache.functionals.append(settings.render().el);
    }
    
    /* Render the function's SVG */
    view = new functionalSVG({model: functional, d3: this.d3});
  },
  
  // mouse move: update text of coordinates, update tangency points
  move: function(e) {
    var g = $('#graph'), svgX, svgY, x, y, diff, lastIndex, fixed;
    
    // Get coordinates
    svgX = e.pageX - (g.offset().left + parseInt(g.css('borderLeftWidth')));
    svgY = e.pageY - (g.offset().top + parseInt(g.css('borderTopWidth')));
    x = this.d3.xScale.invert(svgX - this.model.get('dim').m);
    y = this.d3.yScale.invert(svgY - this.model.get('dim').m);
    
    // Update the text of coordinates
    diff = Math.abs(x - this.d3.xScale.invert(svgX + 1 - this.model.get('dim').m)).toExponential();
    lastIndex = diff.lastIndexOf('-');
    fixed = lastIndex == -1 ? 1 : Math.abs(parseInt(diff.substr(lastIndex)));
    this.cache.coordinates.text('('+x.toFixed(fixed)+', '+y.toFixed(fixed)+')');
    
    // Update the tangency point for each functional
    this.collection.each(function(functional) {
      //if(functional.inDomain(x)) {
        functional.set('tangent_point', x);
      //}
    });
  }
})

/* ----------------------------------------------------------------------------------- */

$(function() {
  // Old IE versions can't handle this!
  if(($.browser.msie && parseInt($.browser.version, 10) < 9)) {
    // Remove the "Loading" div, hide the tabs
    $('#graph')
        .add('#add-functional-container')
        .add('#best-viewed-in')
        .add('#toggle-help')
      .remove();
    // Display the compatibility div
    $('#compatibility').show();
    return;
  }
  
  // Display the "best used with Chrome" message for Chrome users.
  if(!/chrome/i.test(navigator.userAgent)) {
    $('#best-viewed-in').show();
  }
  
  // If we have a new enough browser, initialize the graphing
  graph = new GraphModel;
  funcs = new functionals([
    {'color':'#FF0000', 'parameters': new parameters([{value: 0}, {value: 0}, {value: 1}])}
  ]);
  Studio = new StudioView({model: graph, collection: funcs});  
  
  // Setup the help "tooltips"
  var help = $('.help');
  var toggle = $('#toggle-help');
  var tooltips = $('.tooltip');
  
  // Setup the tooltips initially
  var gravities = ['nw', 'n', 'ne', 'w', 'e', 'sw', 's', 'se'];
  for(i in gravities) {
    tooltips.filter('.tooltip-'+gravities[i])
      .tipsy({fade: true, gravity: gravities[i], trigger: 'manual'});
  }
  
  // Event for toggling tooltips
  toggle.bind('click', function(e) {
    e.preventDefault();
    var f = $('.functional');
    
    if(toggle.text() == 'Show tips / help') {
      // Show the basic tooltips
      tooltips.each(function(i, e) { $(e).tipsy('show'); } );
      
      // If we have functionals, then there's another set of tips to show
      if(f.length > 0) {
        toggle.text('Next tips / help');
      }
      // Otherwise, have the next click close the tips altogether
      else {
        toggle.text('Hide tips / help');
      }
    }
    else if(toggle.text() == 'Next tips / help') {
      // Hide the basic tooltips
      tooltips.each(function(i, e) { $(e).tipsy('hide'); } );
      
      // If we have at least one functional, then generate tooltips for it
      if(f.length > 0) {
        f = f.first();
        
        // Functional form
        $('select.functional-form', f).attr('title', 'View / change the functional form.')
          .tipsy({fade: true, gravity: 's', trigger: 'manual'})
          .tipsy('show');
        // Delete functional
        $('a.remove-functional', f).attr('title', 'Remove the function.')
          .tipsy({fade: true, gravity: 'se', trigger: 'manual'})
          .tipsy('show');
        // Color
        $('div.color a', f).attr('title', "Change the function's color.")
          .tipsy({fade: true, gravity: 'ne', trigger: 'manual'})
          .tipsy('show');        
        // Equations
        $('div.equations', f).attr('title', "The equation(s) describing the function")
          .tipsy({fade: true, gravity: 'e', trigger: 'manual'})
          .tipsy('show');
        // Derivative / tangent line
        $('label.show-tangent', f).attr('title', "These toggles show / hide the function's derivative or tangent.")
          .tipsy({fade: true, gravity: 'w', trigger: 'manual'})
          .tipsy('show');
        // Change parameters
        $('div.parameter a.remove-parameter').first().attr('title', "Change the parameter's values, either by typing a specific value or by clicking and dragging the slider.")
          .tipsy({fade: true, gravity: 'e', trigger: 'manual'})
          .tipsy('show');
        // Generate Data
        $('a.generate-data').attr('title', "You can generate example datapoints for which the function is the best fitting curve.")
          .tipsy({fade: true, gravity: 'ne', trigger: 'manual'})
          .tipsy('show');
        // Add parameter
        $('a.add-parameter').attr('title', "Expand the functional form by adding another term.")
          .tipsy({fade: true, gravity: 'e', trigger: 'manual'})
          .tipsy('show');
        // Generate Datapoints
        $('div.generate-data-settings').attr('title', "You can generate a set of datapoints, normally distributed around the current function with the standard deviation given.")
          .tipsy({fade: true, gravity: 'e', trigger: 'manual'})
          .tipsy('show');
        // Show Regression
        $('div.regress-data-settings').attr('title', "Then, you can perform a linear regression on the generated datapoints to see how close the estimated values are to the original function.")
          .tipsy({fade: true, gravity: 'n', trigger: 'manual'})
          .tipsy('show');
        
        toggle.text('Hide tips / help');
      }
      else {
        toggle.text('Show tips / help');
      }
    }
    else {
      // If we had at least one functional, remove the generated tooltips.
      $('.tipsy').fadeOut(300, function() { $(this).remove() });
      
      toggle.text('Show tips / help');
    }
  });
  
});