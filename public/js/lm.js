/**
 * Linear Regression Model
 *
 * @param y [y, y, ...] (array of values)
 * @param X [ [x1], [x2], ... ] (array of columns)
 * @param ['y', '_cons', 'x', 'x<sup>2</sup>', ... ] (array of strings)
 */
var lm = function(y, tX, terms) {
  var n, k, X, tX, tXX, tXXi, b, yhat, e, iota, M0, SSE, SST, SSR,
      dof, s2, vcov, se, tstat, p, tmp, tmp2;
      
  // If it's just a column, Sylvester incorrectly makes it into a column
  tX = $M(tX);
  this.terms = terms;
  this.n = n = y.length;
  this.y = y = $V(y);
  this.X = X = tX.transpose(); // we're given data in the opposite orientation
  this.k = k = X.cols();
  
  // perform linear regression
  tXX = tX.multiply(X);
  // if it's a 1x1 matrix, Sylvester has problems, so manually invert it
  tXXi = X.cols() > 1 ? tXX.inverse() : $M([1/tXX.e(1,1)]);
  if(tXXi == null) throw 'Singular matrix';
  
  this.b = b = tXXi.multiply(tX).multiply(y);
  this.e = e = y.subtract(X.multiply(b));
  this.yhat = yhat = y.subtract(e);
  
  // Sums of squares:
  iota = $M(Vector.Zero(n).map(function() { return 1; }));
  M0 = Matrix.I(n).subtract(iota.multiply(iota.transpose()).multiply(1/n));
  this.SSE = SSE = e.dot(e);
  this.SST = SST = $M(y).transpose().multiply(M0).multiply($M(y)).e(1,1);
  this.SSR = SSR = SST - SSE;
    
  // degrees of freedom
  this.dof = dof = (n - k);
  // get estimate for variance
  this.s2 = s2 = SSE / dof;
  // get variance/covariance matrix
  this.vcov = vcov = tXXi.multiply(s2);
  // get std. errors
  this.se = se = $V(_.map(vcov.diagonal().elements, function(x) {
    return Math.pow(x, 0.5);
  }));
  // get tstats
  this.tstat = tstat = $V(_.map(se.elements, function(x, i) {
    return b.e(i+1) / x;
  }));
  // get p values
  this.p = p = $V(_.map(tstat.elements, function(x) {
    return jStat.studentt.cdf(-1*Math.abs(x), dof)*2;
  }));
  
  // get R^2, adjusted R^2
  this.r2 = 1 - SSE/SST;
  this.adj_r2 = 1 - (SSE / (n - k)) / (SST / (n - 1));
  
  // get F-stat, p value
  tmp = $V(M0.multiply($M(yhat)));
  tmp2 = yhat.subtract(y)
  this.f = (tmp.dot(tmp) / (k-1)) / (tmp2.dot(tmp2) / (n-k));
  this.f_df1 = k-1;
  this.f_df2 = n-k;
  this.f_p = 1-jStat.centralF.cdf(this.f, k-1, n-k);
}
// Get an object representing the coefficient at position i (0-indexed)
lm.prototype.coef = function(i) {
  var p;
  i = i+1; // change to 1-indexing for Sylvester
  p = this.p.e(i);
  return p === null ? null : {
    'term':  this.terms[i], // 1-indexed (b/c 'y' is the 0th element)
    'b':     this.b.e(i),
    'se':    this.se.e(i),
    'tstat': this.tstat.e(i),
    'p':     p,
    'ci_l':  this.b.e(i) - 1.96*this.se.e(i),
    'ci_u':  this.b.e(i) + 1.96*this.se.e(i),
    '1':     p != null && p <= 0.01,
    '5':     p != null && p <= 0.05,
    '10':    p != null && p <= 0.1
  };
}
lm.prototype.coefs = function() {
  var b = [], i=0, c;
  while(c=this.coef(i++)) {
    b.push(c);
  }
  return b;
}
lm.prototype.summary = function() {
  return {
    'adj_r2': this.adj_r2,
    'b':      this.coefs(),
    'dep':    this.terms[0],
    'f':      this.f,
    'f_p':    this.f_p,
    'f_df1':  this.f_df1,
    'f_df2':  this.f_df2,
    'n':      this.n,
    'r2':     this.r2,
    's2':     this.s2,
    'SSE':    this.SSE, // sum of squared errors
    'SSR':    this.SSR, // regression sum of squares
    'SST':    this.SST, // total sum of squares
  }
}
