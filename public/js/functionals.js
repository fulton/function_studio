/**
 * Models
 */
var parameter = Backbone.Model.extend({
  defaults: { value: 0 }
});

var functional = Backbone.Model.extend({
  defaults: function() {
    return {
      color: '#000000',
      label: 'Polynomial',
      max_parameters: 0,
      parameters: new parameters([{value: 0}, {value: 1}]),
      type:'polynomial',
      show_derivative:false,
      show_tangent: false,
      tangent_point: 0,
      
      // for the generated datapoints
      datapoints: [], // an array of [x,y] coordinates
      distribution: null, // the distribution for x datapoints
      number_datapoints: 100,
      lm: {}, // results of a linear regression on the datapoints dataset
      regression: null, // a functional instance of the regression line
      show_regression: false, // whether or not to show the regression line
      sd: 1, // the standard deviation for the distribution of deviations from perfect y values
      show_datapoints: false // whether or not to show datapoints
    }
  },
  initialize: function() {
    // @todo refactor getDistribution into a static property rather than a method
    // @todo refactor this somehow, but note that a static property won't work since the
    //       visible frame changes when the parameters change, so it has to update
    //this.set('distribution', functions['polynomial'].getDistribution.call(this));
  
    // when the type changes
    this.bind('change:type', function() {
      var type = this.get('type'), r = this.get('regression');
      this.set('label', functions[type].label);
      this.set('max_parameters', functions[type].max_parameters);
      this.set('distribution', functions[type].getDistribution.call(this));
      if(this.get('show_datapoints')) {
        this.generateDatapoints();
        
        if(this.get('show_regression') && r !== null) {
          r.set('type', type);
        }
      }
    }, this);
    
    // we need to update the datapoints:
    // - when the parameters change
    this.get('parameters').bind('all', function(e) { // when parameters change
      if(this.get('show_datapoints')) {
        this.generateDatapoints();
      }
    }, this);
    // when the sd or number of datapoints change
    this.bind('change:number_datapoints change:sd change:show_datapoints', function(e) {
      if(!this.get('show_datapoints')) this.set('show_regression', false);
      
      this.generateDatapoints();
    }, this);
    
    // we need to update the regression
    this.bind('change:show_regression change:datapoints', function(e) {
      var r = this.get('regression');
      if(this.get('show_regression')) {
        // Create the regression functional, if it doesn't exist
        // (can't create it on initialization or we'll have an infinite recursion)
        if(r === null) {
          r = new functional({
            color: d3.rgb(this.get('color')).darker(2).toString(),
            label: this.get('label'),
            type: this.get('type')
          });
          this.set('regression', r);
          Studio.add(r, false); // adds the SVG
          Studio.collection.add(r, {silent:true});
        }
      
        this.regress();
      }
      else if(r !== null) {
        r.destroy(); // removes the SVG
        this.set('regression', null);
      }
      updating = false;
    }, this);
    
    // when the lm updates, we need to update the regression functional
    this.bind('change:lm', function(e) {
      var r = this.get('regression'), lm = this.get('lm'), coefs = lm.coefs(), parameters = [];
      if(this.get('show_regression') && r !== null && lm != null) {
        for(i=0;i<coefs.length;i++) {
          parameters.push({value:coefs[i].b});
        }
        r.get('parameters').reset(parameters);
      }
    }, this);
    
    // update the regression functional's color when this functional's color updates
    this.bind('change:color', function(e) {
      var r = this.get('regression');
      if(r !== null) {
        r.set('color', d3.rgb(this.get('color')).darker(2).toString());
      }
    });
    
    this.bind('destroy', function(e) {
      var r = this.get('regression')
      if(r) r.destroy();
    });
    
  },
  domain: function(x1, x2, setting) {
    var points, type = this.get('type');
    setting = setting || 'static'; // possible values: static, dynamic, zoom
    
    if(functions[type].domain != undefined) {
      return functions[type].domain.call(this, x1, x2, setting);
    }
    else {
      points = (setting == 'static') ? 1000 : 300;
      return _.range(x1, x2, Math.abs(x2-x1)/points);
    }
  },
  dydx: function(x) {
    var type = this.get('type');
    return functions[type].dydx.call(this, x);
  },
  generateDatapoints: function() {
    var x = [], y = [], n, dist = this.getDistribution(), sample, norm;
    if(!this.get('show_datapoints')) {
      if(this.get('datapoints').length > 0) {
        this.set('datapoints', []);
      }
      return this.get('datapoints');
    }
    
    // Setup the x values
    n = this.get('number_datapoints');
    for(i=0; i<n; i++) {
      sample = dist.sample();
      // we need the value to be in the domain of the function, though
      if(this.inDomain(sample)) {
        x.push(sample);
      }
      else {
        i--;
      }
    }
    
    // Start with perfectly fitted y values
    y = _.map(x, this.map, this);
    
    // Now, generate random deviations, with the given standard deviation (and such that
    // the *deviation* has a mean of 0).
    norm = jStat.normal(0, this.get('sd'));
    for(i=0;i<y.length;i++) {
      sample = norm.sample();
      // we need to make sure the value's in the range of the function, though (e.g. log-log)
      if(this.inRange(y[i] + sample)) {
        y[i] += sample;
      }
      else {
        i--;
      }
    }
    
    this.set('datapoints', _.zip(x,y));
    return this.get('datapoints');
  },
  getDistribution: function() {
    var type = this.get('type');
    return functions[type].getDistribution.call(this); // @todo cache this?
  },
  // trims the ends of the domain, (not the non-visible parts in the middle)
  getVisibleDomain: function(x1, x2, y1, y2, step) {
    var domain = [x1, x2], x, y;
    
    step = step || 0.1;
    
    // eliminate all x's that are not visible going forwards
    for(x=x1;x<x2;x+=step) {
      if(!this.inDomain(x)) continue;
      y = this.map(x);
      
      domain[0] = x;
      if(y > y1 && y < y2) break;
    }
    // eliminate all x's that are not visible going backwards
    for(x=x2;x>x1;x-=step) {
      if(!this.inDomain(x)) continue;
      y = this.map(x);
      
      domain[1] = x;
      if(y > y1 && y < y2) break;
    }
    
    return domain;
  },
  inDomain: function(x) {
    var type = this.get('type');
    return functions[type].inDomain.call(this, x);
  },
  inRange: function(y) {
    var type = this.get('type');
    return functions[type].inRange.call(this, y);
  },
  initialBounds: function() {
    var type = this.get('type');
    return functions[type].initialBounds.call(this);
  },
  map: function(x) {
    var type = this.get('type');
    return functions[type].map.call(this, x);
  },
  regress: function()  {
    var type = this.get('type'), lm = {};
    
    try {
      lm = functions[type].regress.call(this, this.get('datapoints'));
    }
    catch(err) {
      console.log('regression error: '+err);
    }
    
    this.set('lm', lm);
    // it doesn't trigger the change when the regression turns up the same results
    // (i.e. when someone just toggled them off and then on), so we need to manually
    // trigger them
    this.trigger('change:lm');
    return lm;
  },
  tangent: function(x) {
    var y, m, b;
    y = this.map(x);  // y-value
    m = this.dydx(x); // slope
    b = y - m*x;      // y-intercept
    
    return function(x) {
      return m*x + b;
    }
  },
  term: function(i) {
    var type = this.get('type');
    return functions[type].term.call(this, i);
  },
  toString: function(which) {
    var type = this.get('type');
    return functions[type].toString.call(this, which);
  },
});

/**
 * Collections
 */
var parameters = Backbone.Collection.extend({
  model: parameter
});

var functionals = Backbone.Collection.extend({
  model: functional
});

 
/**
 * Views
 */
var parameterSettings = Backbone.View.extend({
  className: "parameter",
  template: _.template($('#parameter-template').html()),
  events: {
    'change input': 'update',
    'click .remove-parameter': 'clear'
  },
  
  initialize: function() {
    this.options.bound = 20; // @TODO replace with parameter-specific bounds
  
    // model events
    this.model.bind('change', this.update, this);
    this.model.bind('destroy', this.remove, this);
  },
  
  render: function() {
    var self = this, slider;
    
    this.$el.html(this.template({
      parameter: String.fromCharCode(65+this.options.index).toLowerCase(),
      value: this.model.get('value'),
      lbound: -this.options.bound,
      rbound: this.options.bound
    }));
    
    // create the slider
    slider = $('<div class="slider"></div>').slider({
      animate: false,
      min: -self.options.bound,
      max: self.options.bound,
      step: 0.01,
      value: self.model.get('value'),
      slide: function(e) {
        self.update(e)
      },
      change: function(e) {
        self.update(e)
      }
    });
    this.$el.append(slider);
    
    return this;
  },
  
  update: function(e) {
    var slider = this.$('.slider'), value;
    // We test for originalEvent, since the lines "this.$('input').val(value)" and
    // "this.slider.slider('value', value)" will trigger change and slidechange events,
    // respectively, which could lead to an infinite nest of calls unless we exclude them.
    if(e && e.originalEvent) {
      // If the slider got moved (note: we need both, because if the
      // slider is clicked rather than dragged, then the 'slide' event will actually
      // trigger before the slider 'value' is updated.
      if(e.type == 'slide' || e.type == 'slidechange') {
        value = slider.slider('value');
        this.$('input').val(value)
      }
      // If the input element changed
      else if(e.type == 'change') {
        value = this.$('input').val()
        switch(value) {
          case 'e':
          case 'E':
            value = Math.E;
            break;
          case 'Pi':
          case 'PI':
          case 'pi':
            value = Math.PI;
            break;
        }
        slider.slider('value', value)
      }
      
      this.model.set({'value': value*1});
    }
    // If the model itself was programmatically updated, here we just need to update
    // the slider and the input
    else if(e instanceof Backbone.Model) {
      value = e.get('value');
      
      if(value == Math.E) value = 'e';
      if(value == Math.PI) value = 'pi';
      
      this.$('input').val(value)
      
      if(!isNaN(value)) {
        slider.slider('value', value)
      }
    }
  },
  
  clear: function(e) {
    e.preventDefault();
    
    // If the parameter has a value != 0 then set it to 0
    if(this.model.get('value') != 0) {
      this.model.set('value', 0);
    }
    // If there are a variable number of parameters, and it's the last one, destroy it
    else if(this.options.functional.get('max_parameters') == 0) {
      var parameters = this.$el.parent().find('.parameter');
      var index = parameters.index(this.$el)
      
      if(index+1 == parameters.length) {
        this.model.destroy();
      }
    }
  }
});

var generateSettings = Backbone.View.extend({
  className: "generate-data",
  template: _.template($('#generate-data-template').html()),
  lm_template: _.template($('#lm-results-template').html()),
  events: {
    'change .number-datapoints input': 'update',
    'click .show-regression input': 'update',
    'click .toggle-datapoints': 'update',
    'change .sd input': 'updateSlider'
  },
  toggleLock: false, // lock on toggling show_regression too quickly
  
  initialize: function() {
    // model events
    this.model.bind('change:show_datapoints change:number_datapoints change:show_regression', this.render, this);
    this.model.bind('change:lm', this.renderRegression, this);
    this.model.bind('change:sd', this.updateSlider, this);
    this.model.bind('destroy', this.remove, this);
  },
  
  render: function() {
    var self = this, slider, terms = {}, json;
    
    this.$el.html(this.template(this.model.toJSON()));
    this.$('a').button();
    
    // create the slider
    slider = $('<div class="slider"></div>').slider({
      animate: false,
      min: 0,
      max: 50,
      step: 1,
      value: self.model.get('sd'),
      slide: function(e) {
        self.updateSlider(e)
      },
      change: function(e) {
        self.updateSlider(e)
      }
    });
    this.$('div.generate-data-settings').append(slider);
    
    this.renderRegression();
    
    return this;
  },
  
  renderRegression: function() {
    var lm = this.model.get('lm');
    this.$('div.lm-results').remove();
    if(lm.summary && this.model.get('show_regression')) {
      this.$('div.regress-data-settings').append(
        this.lm_template(this.model.get('lm').summary())
      );
    }
    return this;
  },
  
  updateSlider: function(e) {
    var slider = this.$('.slider'), value;
    // We test for originalEvent, since the lines "this.$('input').val(value)" and
    // "this.slider.slider('value', value)" will trigger change and slidechange events,
    // respectively, which could lead to an infinite nest of calls unless we exclude them.
    if(e && e.originalEvent) {
      // If the slider got moved (note: we need both, because if the
      // slider is clicked rather than dragged, then the 'slide' event will actually
      // trigger before the slider 'value' is updated.
      if(e.type == 'slide' || e.type == 'slidechange') {
        value = slider.slider('value');
        this.$('.sd input').val(value)
      }
      // If the input element changed
      else if(e.type == 'change') {
        value = this.$('.sd input').val()
        slider.slider('value', value)
      }
      
      this.model.set('sd', value*1);
    }
    // If the model itself was programmatically updated, here we just need to update
    // the slider and the input
    else if(e instanceof Backbone.Model) {
      value = e.get('sd');
      
      this.$('.sd input').val(value)
      
      if(!isNaN(value)) {
        slider.slider('value', value)
      }
    }
  },
  // generic updater
  update: function(event) {
    var self = this, e = $(event.currentTarget), label = e.parent('label');
    
    if(e.hasClass('toggle-datapoints')) {
      event.preventDefault();
      this.model.set('show_datapoints', !this.model.get('show_datapoints'));
    }
    else if(label.hasClass('number-datapoints')) {
      this.model.set('number_datapoints', e.val());
    }
    else if(label.hasClass('show-regression')) {
      if(this.toggleLock) {
        event.preventDefault();
        return false;
      }
      else {
        this.model.set('show_regression', e.is(':checked'));
        
        this.toggleLock = true;
        setTimeout(function() {
          self.toggleLock = false;
        }, 150);
      }
    }
  }
});

var functionalSettings = Backbone.View.extend({
  className: "functional",
  templates: {
    'add_parameter': _.template($('#add-parameter-template').html()),
    'function_template': _.template($('#function-template').html()),
    'equations_template': _.template($('#equations-template').html()),
  },
  events: {
    'change select': 'change_type',
    'click .add-parameter': 'create',
    'click .remove-functional': 'clear',
    'change .show-derivative': 'show_derivative',
    'change .show-tangent': 'show_tangent',
  },
  parameters: null, // array of parameterSettings views
  generate: null, // the generate-data view
  
  initialize: function() {
    this.parameters = [];
    // set up the parameters
    var parameters = this.model.get('parameters');
    parameters.each(function(parameter, index) {
      this.add(parameter, false);
    }, this);
    // set up generate data
    this.generate = new generateSettings({model: this.model});
  
    // model events
    this.model.bind('change:color change:show_derivative change:show_tangent change:tangent_point', this.render, this);
    parameters.bind('add', this.add, this);
    parameters.bind('remove', function(e) {
      this.parameters.pop();
    }, this);
    parameters.bind('change', function() {
      this.$('.equations').empty()
        .append(this.templates.equations_template($.extend(this.model.toJSON(), {
            'tex_map': this.model.toString(),
            'tex_dydx': this.model.toString('dydx'),
            'tex_tangent': this.model.toString('tangent')
        })));
    }, this);
    this.model.bind('destroy', this.remove, this);
  },
  
  render: function() {
    var self = this, view, max_p;
    
    //add the settings
    this.$el.empty()
            .append(this.templates.function_template(this.model.toJSON()));
    // add the equations
    this.$('.equations')
        .append(this.templates.equations_template($.extend(this.model.toJSON(), {
            'tex_map': this.model.toString(),
            'tex_dydx': this.model.toString('dydx'),
            'tex_tangent': this.model.toString('tangent'),
        })));
    // create the color input
    this.$el.find('.colorinput').miniColors({
      hide: function(hex, rgb) {
        if(hex != self.model.get('color')) {
          $(this).miniColors('destroy');
          self.model.set('color', hex);
        }
      }
    });
        
    // add the parameter editors
    _.each(this.parameters, function(view, index) {
      // @todo this shouldn't be necessary, but if it's not included, when new
      //       parameters are added, all events on existing views are undelegated...
      view.delegateEvents();
      
      this.$el.append(view.render().el); // note: not re-rendering here
    }, this);
    
    // add the add-parameter button
    max_p = this.model.get('max_parameters');
    if(!max_p || this.model.get('parameters').length < max_p) {
      add = $(this.templates.add_parameter());
      add.button()
      this.$el.append(add);
    }
    
    // add the generate-data section
    this.generate.delegateEvents(); // similar to above, events get undelegated
    this.$el.append(this.generate.render().el);
    
    return this;
  },
  
  create: function(e) {
    e.preventDefault();
    this.model.get('parameters').add();
  },
  
  add: function(parameter, render) {
    this.parameters.push(new parameterSettings({
      model: parameter,
      index: this.parameters.length,
      functional: this.model
    }).render());
    
    if(render) {
      this.render();
    }
  },
  
  clear: function(e) {
    e.preventDefault();
    this.model.destroy();
  },
  
  change_type: function(e) {
    var type, max_parameters, parameters;
    type = $(':selected', e.target).val();
    
    this.model.set('type', type);
    
    // Remove any extra parameters
    max_parameters = this.model.get('max_parameters');
    if(max_parameters > 0) {
      parameters = this.model.get('parameters');
      while(parameters.length > max_parameters) {
        parameters.pop();
      }
    }
    
    this.render();
  },
  
  show_derivative: function(e) {
    this.model.set('show_derivative', e.target.checked)
  },
  
  show_tangent: function(e) {
    this.model.set('show_tangent', e.target.checked)
  }
});

/**
 * Functional SVG Representation
 *
 * Only handles drawing the actual lines, and datapoints
 */
var functionalSVG = Backbone.View.extend({
  dydx: null, // the derivative path
  tangent: null, // the tangent line
  tangency: null, // the circle showing the slope of the tangent line
  datapoints: null, // generated datapoints
  
  // cached cutoffs from the last time a render was performed, to be checked against to
  // prevent unnecessary re-rendering
  xCutoff: [0,0],
  yCutoff: [0,0],
  
  initialize: function() {
    var self = this,
        xDomain = this.options.d3.xScale0.domain(),
        yDomain = this.options.d3.yScale0.domain(),
        xDiff = xDomain[1] - xDomain[0],
        yDiff = yDomain[1] - yDomain[0],
        data, path, tangency, tangent, line, circle,
        finalize;
    
    // Render the function and derivative
    this.render('static');
    
    // Render the tangent line and slope point
    tangency = this.model.get('tangent_point');
    tangent = this.model.tangent(tangency);
    points = [
      [tangency - 2, tangent(tangency - 2)],
      [tangency + 2, tangent(tangency + 2)] 
    ];
    line = this.options.d3.curves
                      .append('line')
                      .attr('class', 'line curve tangent')
                      .style('stroke', d3.rgb(self.model.get('color')).darker(2).toString())
                      .style('display', this.model.get('show_tangent') ? '' : 'none')
                      .attr('x1', this.options.d3.xScale0(points[0][0]))
                      .attr('y1', this.options.d3.yScale0(points[0][1]))
                      .attr('x2', this.options.d3.xScale0(points[1][0]))
                      .attr('y2', this.options.d3.yScale0(points[1][1]));
    circle = this.options.d3.curves
                      .append('circle')
                      .attr('class', 'tangent')
                      .style('fill', d3.rgb(self.model.get('color')).darker(2).toString())
                      .style('display', this.model.get('show_tangent') ? '' : 'none')
                      .attr('cx', this.options.d3.xScale0(tangency))
                      .attr('cy', this.options.d3.yScale0(this.model.dydx(tangency)))
                      .attr('r', 3)
    this.tangent = line[0][0]
    this.tangency = circle[0][0]
    
    // Color change effects all elements
    this.model.bind('change:color', function(e) {
      d3.select(this.el)
        .style('stroke', self.model.get('color'))
      d3.select(this.dydx)
        .style('stroke', d3.rgb(self.model.get('color')).darker().toString());
      d3.select(this.tangent)
        .style('stroke', d3.rgb(self.model.get('color')).darker().toString());
      d3.select(this.tangency)
        .style('fill', d3.rgb(self.model.get('color')).darker().toString());
        
      this.options.d3.g.selectAll('text.dp_'+this.model.cid)
        .style('fill', d3.rgb(self.model.get('color')).darker(2).toString())
    }, this);
    
    // Toggles for derivatives and tangent lines (note that when their display is set
    // to none, they don't contribute to render times)
    this.model.bind('change:show_derivative', function(e) {
      d3.select(this.dydx)
        .style('display', this.model.get('show_derivative') ? '' : 'none');
    }, this);
    this.model.bind('change:show_tangent', function(e) {
      d3.select(this.tangent)
        .style('display', this.model.get('show_tangent') ? '' : 'none');
      d3.select(this.tangency)
        .style('display', this.model.get('show_tangent') ? '' : 'none');
    }, this);
    this.model.bind('change:tangent_point', this.renderTangent, this);
    
    // Zoom / pan events (triggered by studio.js)
    this.model.bind('zoom', function(e) {
      this.render('dynamic', 'zoom');
      clearTimeout(finalize);
      finalize = setTimeout(function() {
        self.render('static', 'zoom');
      }, 200);
    }, this);
    // Parameter-change events
    this.model.get('parameters').bind('all', function(e) {
      this.render('dynamic', 'parameter');
      clearTimeout(finalize);
      finalize = setTimeout(function() {
        self.render('static', 'parameter');
      }, 200);
    }, this);
    // If the functional's type changes
    this.model.bind('change:type', function(e) {
      this.render('static', 'parameter');
    }, this);
    
    // Data generation initialization / events
    // (use the model's cid - unique and automatically generated - to identify the circles)
    this.datapoints = this.options.d3.g.select('circle.dp_'+this.model.cid);
    this.model.bind('change:datapoints', this.renderData, this);
    
    // If the model is removed, get rid of all the SVG elements
    this.model.bind('destroy', function() {
      $(this.dydx).remove();
      $(this.tangent).remove();
      $(this.tangency).remove();
      this.options.d3.datapoints.selectAll('text.dp_'+this.model.cid).remove();
      this.remove()
    }, this);
  },
  
  render: function(setting, type, duration) {
    var self = this,
        xDomain = this.options.d3.xScale.domain(),
        yDomain = this.options.d3.yScale.domain(),
        xDiff = xDomain[1] - xDomain[0],
        yDiff = yDomain[1] - yDomain[0],
        
        // the cutoffs define how much of each function we're pre-rendering. The trade
        // off is that the more we pre-render the faster it will be to perform (local)
        // zoom and pan operations, but the slower it will be to perform parameter/
        // update operations
        xCutoff = [ // in graph-units
          xDomain[0]-0.5*xDiff,
          xDomain[1]+0.5*xDiff
        ],
        yCutoff = [ // in graph-units
          yDomain[0]-0.5*yDiff,
          yDomain[1]+0.5*yDiff
        ],
        x, fx, dydx, tmp, data, el, stroke_width;
    // default for setting is "static"
    setting = setting || 'dynamic';
    duration = duration || 0;
    
    // check if we need to re-render
    if(type != 'parameter' && setting != 'static' &&
       !(xDomain[0] < this.xCutoff[0] || xDomain[1] > this.xCutoff[1] ||
         yDomain[0] < this.yCutoff[0] || yDomain[1] > this.yCutoff[1])
    ) {
      // we need to still re-render the tangent line
      this.renderTangent();
      return;
    }
    
    this.xCutoff = xCutoff;
    this.yCutoff = yCutoff;
    
    // get the data
    x = this.model.domain(xCutoff[0], xCutoff[1], setting);
    fx = _.map(x, this.model.map, this.model);
    dydx = _.map(x, this.model.dydx, this.model),
    
    /* ----------------------------------- Function ----------------------------------- */
    
    // Select the current element
    el = this.el ? d3.select(this.el) : null;
    
    // only plot those points where the y-value is visible
    tmp = _.zip(x, fx);
    data = _.filter(
      tmp,
      function(d, i) {
        // remember that these are in graph-units, so we want it to be more than the
        // lowest value (yCutoff[0]) and less than the highest value (yCutoff[1])
        var pass = [
          tmp[i-1] == undefined || (tmp[i-1][1] >= yCutoff[0] && tmp[i-1][1] <= yCutoff[1]),
          tmp[i+1] == undefined || (tmp[i+1][1] >= yCutoff[0] && tmp[i+1][1] <= yCutoff[1])
        ];
        
        return (pass[0] && tmp[i-1] != undefined) || (pass[1] && tmp[i+1] != undefined);
      }
    );
    
    // If we're rendering a static path, then we re-draw the entire path
    // (this prevents visual artifacts created by transitioning from a path with fewer
    // points to one with more points)
    if(!el || setting == 'static') {
      stroke_width = 1.1/this.options.d3.scale || 1.1;
      // draw the new path (function)
      if(el) { // clear the old path, if it exists
        // @todo this is obsolete now, but probably should be removed with some general
        //       refactoring of this section, given the static stroke widths
        //stroke_width = el.style('stroke-width');
        el.remove();
      }
      path = this.options.d3.curves
                            .data([data])
                              .append('path')
                            .attr('class', 'line curve function')
                            .style('stroke-width', stroke_width+'px')
                            .style('stroke', self.model.get('color'))
                            .attr('d',
                              d3.svg.line().x(function(d) {
                                return self.options.d3.xScale0(d[0]);
                              }).y(function(d) {
                                return self.options.d3.yScale0(d[1]);
                              })
                            );
                            
      // we need to manually set the View's element, since jQuery appends using
      // innerHTML(), which does not work with svg namespaced elements like svg:g.
      this.setElement(path[0]);
    }
    // Otherwise (for dynamic or zoom renders) just transition the existing path
    else {
      // update the path (function)
      el.data([data])
        .transition().duration(duration)
          .attr('d',
            d3.svg.line().interpolate('linear').x(function(d) {
              return self.options.d3.xScale0(d[0]);
            }).y(function(d) {
              return self.options.d3.yScale0(d[1]);
            })
          );
    }
    
    /* ---------------------------------- Derivative ---------------------------------- */
    
    // Get the derivative path, if it exists
    el = this.dydx ? d3.select(this.dydx) : null;
    tmp = _.zip(x, dydx);
    data = _.filter(
      tmp,
      function(d, i) {
        // remember that these are in graph-units, so we want it to be more than the
        // lowest value (yCutoff[0]) and less than the highest value (yCutoff[1])
        var pass = [
          tmp[i-1] == undefined || (tmp[i-1][1] >= yCutoff[0] && tmp[i-1][1] <= yCutoff[1]),
          tmp[i+1] == undefined || (tmp[i+1][1] >= yCutoff[0] && tmp[i+1][1] <= yCutoff[1])
        ];
        
        return (pass[0] && tmp[i-1] != undefined) || (pass[1] && tmp[i+1] != undefined);
      }
    );
    
    if(!el || setting == 'static') {
      stroke_width = 1.1/this.options.d3.scale || 1.1;
      
      // draw the new path (derivative)
      if(el) { // clear the old path, if it exists
        //stroke_width = el.style('stroke-width');
        el.remove();
      }
      path = this.options.d3.curves
                          .data([data])
                            .append('path')
                          .attr('class', 'line curve dydx')
                          .style('stroke-width', stroke_width+'px')
                          //.style('stroke-dasharray', '5,5')
                          .style('stroke', d3.rgb(self.model.get('color')).darker(2).toString())
                          .style('display', this.model.get('show_derivative') ? '' : 'none')
                          .attr('d',
                            d3.svg.line().interpolate('linear').x(function(d) {
                                return self.options.d3.xScale0(d[0]);
                              }).y(function(d) {
                                return self.options.d3.yScale0(d[1]);
                            })
                          );
      // note: need two [][] instead of the one for this.el above, since setElement sets
      // this.$el = path[0] and this.el = path[0][0] automatically.
      this.dydx = path[0][0];
    }
    else {
      // update the path (derivative)
      d3.select(this.dydx)
          .data([data])
        .transition().duration(duration)
          //.style('stroke-dasharray', '')
          .attr('d',
            d3.svg.line().x(function(d) {
              return self.options.d3.xScale0(d[0]);
            }).y(function(d) {
              return self.options.d3.yScale0(d[1]);
            })
          );
    }
    
    // Re-render the tangent line
    if(type == 'parameter') {
      this.renderTangent();
    }

    return this;
  },
  
  renderData: function() {
    var self = this, dist, data, dp;
    
    // get the dataset
    data = this.model.get('datapoints');
    
    // get the svg text elements
    dp = this.options.d3.datapoints.selectAll('text.dp_'+this.model.cid)
      .data(data);
    
    // Add new if necessary
    dp.enter().append('text')
        .attr('class', 'dp dp_'+this.model.cid)
        .attr('x', function(d) { return self.options.d3.xScale0(d[0]); })
        .attr('y', function(d) { return self.options.d3.yScale0(d[1]); })
        .style('fill', d3.rgb(this.model.get('color')).darker(2).toString())
        .style('text-anchor', 'middle')
        .style('font-size', 9/this.options.d3.scale+'px')
        .text('x');
    
    // Update existing
    dp.attr('class', 'dp dp_'+this.model.cid)
      .attr('x', function(d) { return self.options.d3.xScale0(d[0]); })
      .attr('y', function(d) { return self.options.d3.yScale0(d[1]); })
      .style('fill', d3.rgb(this.model.get('color')).darker(2).toString());
    
    // Remove obsolete
    dp.exit().remove();
  },
  
  renderTangent: function() {
    var tangency, tangent, slope, length, xOffset, yOffset, points;
    
    // get the new point of tangency
    tangency = this.model.get('tangent_point');
    
    // make sure it's a valid point in the domain of the function
    if(!this.model.inDomain(tangency)) {
      d3.select(this.tangent).style('display', 'none');
      d3.select(this.tangency).style('display', 'none');
      return;
    }
    else if(this.model.get('show_tangent')) {
      d3.select(this.tangent).style('display', '');
      d3.select(this.tangency).style('display', '');
    }
    
    tangent = this.model.tangent(tangency); // the tangent function
    
    // we want the line to be of length 6, so figure out what the offset of points need
    // to be to get that
    slope = this.model.dydx(tangency);
    angle = Math.atan(slope);
    length = (this.xCutoff[1] - this.xCutoff[0])/4;
    xOffset = Math.cos(angle) * length/2;
    points = [
      [tangency - xOffset, tangent(tangency - xOffset)],
      [tangency + xOffset, tangent(tangency + xOffset)] 
    ];
    
    // for some functions, the tangent point can get unreasonable 
    if(isNaN(points[0][1]) || Math.abs(points[0][1]) > 10000 || isNaN(points[1][1]) || Math.abs(points[1][1]) > 10000) {
      d3.select(this.tangent).style('display', 'none');
      d3.select(this.tangency).style('display', 'none');
      return;
    }
    
    // update the line
    d3.select(this.tangent)
      .attr('x1', this.options.d3.xScale0(points[0][0]))
      .attr('y1', this.options.d3.yScale0(points[0][1]))
      .attr('x2', this.options.d3.xScale0(points[1][0]))
      .attr('y2', this.options.d3.yScale0(points[1][1]));
    d3.select(this.tangency)
      .attr('cx', this.options.d3.xScale0(tangency))
      .attr('cy', this.options.d3.yScale0(this.model.dydx(tangency)))
  }
});
